Implement the Link-State (LS) and the Distance Vector (DV) routing algorithms. The input will be a network data structure stored in a text file.

1. run the 'make' command to create the executable files

2. run the files
	./distance_vector
	./slotted_aloha
