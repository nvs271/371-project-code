#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

const int WIDTH = 5;

int N;
vector<string> numToName;
vector<vector<vector<int>>> table;

void print_table(bool initial=false){
	if(!initial) cout<<"##########"<<endl<<endl;
	for(int i=0;i<N;i++){
		cout<<"Node "<<numToName[i]<<":"<<endl;
		cout<<setw(WIDTH)<<' ';
		for(int j=0;j<N;j++){
			cout<<setw(WIDTH)<<numToName[j];
		}
		cout<<endl;

		for(int j=0;j<N;j++){
			cout<<setw(WIDTH)<<numToName[j];
			for(int k=0;k<N;k++){
				cout<<setw(WIDTH);
				int val=table[i][j][k];
				if(val==-1) cout<<"inf";
				else cout<<val;
			}
			cout<<endl;
		}
		cout<<endl;
	}
}

int main(){
	cout<<"Enter file name: ";
	string fileName; cin>>fileName;

	ifstream in(fileName);

	if(!in.is_open()){
		cout<<"Could not open file \""<<fileName<<"\""<<endl;
		return 0;
	}

	int L; in>>N>>L;

	table.resize(N, vector<vector<int>>(N, vector<int>(N, -1)));

	unordered_map<string, int> nameToNum;
	numToName.resize(N);

	for(int i=0;i<N;i++){
		in>>numToName[i];
		nameToNum[numToName[i]]=i;
		table[i][i][i]=0;
	}

	while(L--){
		string aa,bb; int c;
		in>>aa>>bb>>c;
		int a=nameToNum[aa];
		int b=nameToNum[bb];

		table[a][a][b]=c;
		table[b][b][a]=c;
	}

	print_table(true); // initial table

	bool changed=true;
	int rounds=0;

	while(changed){
		changed=false;
		rounds++;

		// Send updated costs to other nodes
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				if(i==j) continue;
				for(int k=0;k<N;k++){
					table[i][j][k]=table[j][j][k];
				}
			}
		}

		// check if the found a shorter path
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				if(i==j) continue;
				for(int k=0;k<N;k++){
					if(j==k || table[i][i][k]==-1 || table[i][k][j]==-1) continue;

					int val = table[i][i][k] + table[i][k][j];
					if(val < table[i][i][j]){
						table[i][i][j] = val;
						changed=true;
					}
				}
			}
		}

		print_table();
	}

	cout<<"Num rounds: "<<rounds<<endl;	
	return 0;
}