
def main():
	filename = input('Enter the filename: ')
	F = open(filename, 'r')

	N, L = [int(x) for x in F.readline().strip('\n').split(' ')]
	nodeNames = F.readline().strip('\n').split(' ')
	nodeNums = {}
	for index, value in enumerate(nodeNames):
		nodeNums[value]=index

	# creating a 3-D array of NxNxN with all values initialized to 'inf'
	table = [[['inf' for i in range(N)] for j in range(N)] for k in range(N)]

	for i in range(N):
		table[i][i][i] = 0

	for i in range(L):
		a,b,c = F.readline().strip('\n').split(' ')

		a = nodeNums[a]
		b = nodeNums[b]
		c = int(c)

		table[a][a][b] = c
		table[b][b][a] = c


	def show_table(initial_table=False):
		if not initial_table:
			print('##########\n')

		for i in range(N):
			print('Table for node', nodeNames[i],':')
			print('{:<5}'.format(''), end='')
			for x in nodeNames:
				print('{:>5}'.format(x), end='')
			print()

			for j in range(N):
				print('{:<5}'.format(nodeNames[j]), end='')
				for k in range(N):
					print('{:>5}'.format(str(table[i][j][k])), end='')
				print()
			print()

	show_table(initial_table=True)

	somethingChanged = True
	numRounds = 0

	while somethingChanged:
		somethingChanged = False
		numRounds += 1

		# getting the new updated path cost values from other nodes
		for i in range(N):
			for j in range(N):
				if i==j:
					continue
				for k in range(N):
					table[i][j][k] = table[j][j][k]

		# checking if there is a new shorter path after receiving the updated costs from other nodes
		for i in range(N):
			for j in range(N):
				if i == j:
					continue

				for k in range(N):
					if j == k:
						continue

					a = table[i][i][k]
					b = table[i][k][j]

					if a == 'inf' or b == 'inf':
						continue

					if a + b < table[i][i][j]:
						table[i][i][j] = a + b
						somethingChanged = True

		show_table()

	print('Number of rounds:', numRounds)

if __name__ == '__main__':
	main()