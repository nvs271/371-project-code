all: distance_vector slotted_aloha


distance_vector: distance_vector.cpp
	g++ -std=c++11 -o distance_vector distance_vector.cpp

slotted_aloha: slotted_aloha.cpp
	g++ -std=c++11 -o slotted_aloha slotted_aloha.cpp